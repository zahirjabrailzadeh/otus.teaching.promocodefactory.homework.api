﻿using Grpc.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using AutoMapper;

namespace Otus.Teaching.PromoCodeFactory.GrpcServer
{
    public class CustomerService : Customers.CustomersBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IMapper _mapper;

        public CustomerService(
            IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository,
            IMapper mapper
            )
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _mapper = mapper;
        }

        public override async Task<CustomerShortResponseList> GetCustomers(EmptyClass request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();
            var customesResponse = new CustomerShortResponseList();
            customesResponse.CustomerShortResponse.AddRange(_mapper.Map<IEnumerable<CustomerShortResponse>>(customers));
            return await Task.FromResult(customesResponse);
        }

        public override async Task<CustomerResponse> GetCustomer(GetCustomerAsyncRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));
            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, $"Customer with id {request.Id} not found"));

            var response = _mapper.Map<CustomerResponse>(customer);
            return await Task.FromResult(response);
        }

        public override async Task<CustomerResponse> CreateCustomer(CreateOrEditCustomerRequest request, ServerCallContext context)
        {
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds.Select(x => Guid.Parse(x)).ToList());
            Customer customer = _mapper.Map<Customer>(request);
            await _customerRepository.AddAsync(customer);
            CustomerResponse response = _mapper.Map<CustomerResponse>(customer);
            return await Task.FromResult(response);
        }

        public override async Task<CustomerResponse> EditCustomer(EditCustomersAsyncRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));
            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, $"Customer with id {request.Id} not found"));
            var ids = request.Request.PreferenceIds.Select(x => Guid.Parse(x)).ToList();
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(ids);
            _mapper.Map(request.Request, customer);
            await _customerRepository.UpdateAsync(customer);

            var response = _mapper.Map<CustomerResponse>(customer);
            return await Task.FromResult(response);
        }

        public override async Task<EmptyClass> DeleteCustomer(DeleteCustomerAsyncRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));
            if (customer == null)
                throw new RpcException(new Status(StatusCode.NotFound, $"Customer with id {request.Id} not found"));

            await _customerRepository.DeleteAsync(customer);
            return await Task.FromResult(new EmptyClass());
        }
    }
}
