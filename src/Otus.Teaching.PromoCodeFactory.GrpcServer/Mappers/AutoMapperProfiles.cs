﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;

namespace Otus.Teaching.PromoCodeFactory.GrpcServer.Mappers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<CustomerShortResponse, Customer>()
                .ForMember(dst => dst.Id, opt => opt.MapFrom(src => Guid.Parse(src.Id)));

            CreateMap<Customer, CustomerShortResponse>()
                .ForMember(dst => dst.Id, opt => opt.MapFrom(src => src.Id.ToString()));

            CreateMap<CustomerResponse, Customer>()
                .ForMember(dst => dst.Id, opt => opt.MapFrom(src => Guid.Parse(src.Id)));

            CreateMap<Customer, CustomerResponse>()
                .ForMember(dst => dst.Id, opt => opt.MapFrom(src => src.Id.ToString()));

            CreateMap<CreateOrEditCustomerRequest, Customer>()
                .ReverseMap();

            CreateMap<PreferenceResponse, CustomerPreference>()
                .ForMember(dst => dst.PreferenceId, opt => opt.MapFrom(src => Guid.Parse(src.Id)));

            CreateMap<CustomerPreference, PreferenceResponse>()
                .ForMember(dst => dst.Id, opt => opt.MapFrom(src => src.PreferenceId.ToString()));

            CreateMap<Preference, CustomerPreference>()
                .ReverseMap();
        }
    }
}
