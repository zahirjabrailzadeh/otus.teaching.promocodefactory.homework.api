﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.SignalR.Mappers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<CustomerShortResponse, Customer>()
                .ReverseMap();

            CreateMap<CustomerResponse, Customer>()
                .ReverseMap();

            CreateMap<CreateOrEditCustomerRequest, Customer>()
                .ReverseMap();

            CreateMap<PreferenceResponse, CustomerPreference>()
                .ReverseMap();

            CreateMap<Preference, CustomerPreference>()
                .ReverseMap();
        }
    }
}
