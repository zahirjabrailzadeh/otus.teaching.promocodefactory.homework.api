﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.SignalR.Hubs
{
    public class CustomersHub : Hub
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IMapper _mapper;

        public CustomersHub(
            IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository,
            IMapper mapper
            )
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            return response;
        }

        public async Task<CustomerResponse> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            var response = new CustomerResponse(customer);
            return response;
        }

        public async Task<CustomerResponse> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds);

            Customer customer = CustomerMapper.MapFromModel(request, preferences);

            await _customerRepository.AddAsync(customer);
            var response = _mapper.Map<CustomerResponse>(customer);
            return response;
        }

        public async Task<CustomerResponse> EditCustomerAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return null;

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            CustomerMapper.MapFromModel(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);
            var response = _mapper.Map<CustomerResponse>(customer);
            return response;
        }

        public async Task DeleteCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return;

            await _customerRepository.DeleteAsync(customer);
        }
    }
}
