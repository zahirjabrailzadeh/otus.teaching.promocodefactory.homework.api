﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace ApiClients
{
    static class SignalRTestClient
    {
        public static async Task RunSignalRTestClient()
        {
            Console.WriteLine("***START OF SignalR CLIENT TEST***");
            var connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:5003/hubs/customers")
                .WithAutomaticReconnect()
                .Build();

            await connection.StartAsync();

            var customers = await connection.InvokeAsync<IEnumerable<CustomerShortResponse>>("GetCustomersAsync");
            Console.WriteLine("\n\nCustomers: " + JsonSerializer.Serialize(customers));


            var customer = customers.FirstOrDefault();

            var customerById = await connection.InvokeAsync<CustomerResponse>("GetCustomerAsync", customer?.Id);
            Console.WriteLine("\n\nCustomer By Id: " + JsonSerializer.Serialize(customerById));

            Console.WriteLine("\n\nEditing Customer...");
            if (customer != null)
            {
                var request = new CreateOrEditCustomerRequest
                {
                    FirstName = "EditedFirstName",
                    LastName = "EditedLastName",
                    Email = "EditedEmail"
                };

                request.PreferenceIds.AddRange(customerById.Preferences.Select(p => p.Id.ToString()));

                var result = await connection.InvokeAsync<CustomerResponse>("EditCustomerAsync", customer?.Id, request);

                var editedCustomer = await connection.InvokeAsync<CustomerResponse>("GetCustomerAsync", result?.Id);
                Console.WriteLine("\n\nEdited Customer By Id: " + JsonSerializer.Serialize(editedCustomer));
            }

            Console.WriteLine("\n\nAdding new Customer...");
            var createCustomer = new CreateOrEditCustomerRequest
            {
                FirstName = "CreatedFirstName",
                LastName = "CreatedLastName",
                Email = "CreatedEmail"
            };
            createCustomer.PreferenceIds.AddRange(customerById.Preferences.Select(p => p.Id.ToString()));
            var createResult = await connection.InvokeAsync<CustomerResponse>("CreateCustomerAsync", createCustomer);

            customers = await connection.InvokeAsync<IEnumerable<CustomerShortResponse>>("GetCustomersAsync");
            Console.WriteLine("\nCustomers: " + JsonSerializer.Serialize(customers));

            customerById = await connection.InvokeAsync<CustomerResponse>("GetCustomerAsync", createResult?.Id);
            Console.WriteLine("\n\nCustomer By Id: " + JsonSerializer.Serialize(customerById));

            Console.WriteLine("\n\nDeleting new added Customer...");
            await connection.InvokeAsync<Task>("DeleteCustomerAsync", createResult.Id);

            customers = await connection.InvokeAsync<IEnumerable<CustomerShortResponse>>("GetCustomersAsync");
            Console.WriteLine("\nCustomers: " + JsonSerializer.Serialize(customers));

            Console.WriteLine("\n\n***END OF SignalR CLIENT TEST***");
        }
    }
}
