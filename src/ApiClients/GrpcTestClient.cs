﻿using Grpc.Net.Client;
using System;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace ApiClients
{
    static class GrpcTestClient
    {
        public static async Task RunGrpcTestClient()
        {
            Console.WriteLine("***START OF GRPC CLIENT TEST***");
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Customers.CustomersClient(channel);

            var customers = await client.GetCustomersAsync(new EmptyClass());
            Console.WriteLine("\n\nCustomers: " + JsonSerializer.Serialize(customers));

            var customer = customers?.CustomerShortResponse[0];

            var customerById = await client.GetCustomerAsync(new GetCustomerAsyncRequest { Id = customer?.Id });
            Console.WriteLine("\n\nCustomer By Id: " + JsonSerializer.Serialize(customerById));

            Console.WriteLine("\n\nEditing Customer...");
            if (customer != null)
            {
                var request = new EditCustomersAsyncRequest
                {
                    Id = customer.Id,
                    Request = new CreateOrEditCustomerRequest
                    {
                        FirstName = "EditedFirstName",
                        LastName = "EditedLastName",
                        Email = "EditedEmail"
                    }
                };

                request.Request.PreferenceIds.AddRange(customerById.Preferences.Select(p => p.Id.ToString()));

                var result = await client.EditCustomerAsync(request);

                var editedCustomer = await client.GetCustomerAsync(new GetCustomerAsyncRequest { Id = result?.Id });
                Console.WriteLine("\n\nEdited Customer By Id: " + JsonSerializer.Serialize(editedCustomer));
            }

            Console.WriteLine("\n\nAdding new Customer...");
            var createCustomer = new CreateOrEditCustomerRequest
            {
                FirstName = "CreatedFirstName",
                LastName = "CreatedLastName",
                Email = "CreatedEmail"
            };
            createCustomer.PreferenceIds.AddRange(customerById.Preferences.Select(p => p.Id.ToString()));
            var createResult = await client.CreateCustomerAsync(createCustomer);

            customers = await client.GetCustomersAsync(new EmptyClass());
            Console.WriteLine("\nCustomers: " + JsonSerializer.Serialize(customers));

            customerById = await client.GetCustomerAsync(new GetCustomerAsyncRequest { Id = createResult.Id });
            Console.WriteLine("\n\nCustomer By Id: " + JsonSerializer.Serialize(customerById));


            Console.WriteLine("\n\nDeleting new added Customer...");
            await client.DeleteCustomerAsync(new DeleteCustomerAsyncRequest { Id = createResult.Id });
            customers = await client.GetCustomersAsync(new EmptyClass());
            Console.WriteLine("\nCustomers: " + JsonSerializer.Serialize(customers));
            Console.WriteLine("\n\n***END OF GRPC CLIENT TEST***");
        }
    }
}