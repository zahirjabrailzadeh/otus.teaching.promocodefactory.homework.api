﻿
using System.Threading.Tasks;

namespace ApiClients
{
    class Program
    {
        static async Task Main(string[] args)
        {
            await GrpcTestClient.RunGrpcTestClient();
            System.Console.WriteLine("\n\n");
            await SignalRTestClient.RunSignalRTestClient();
        }
    }
}
